
//chart js
$(document).ready(function() {
  var ctxL = document.getElementById("lineChart").getContext('2d');
  console.log('abc');
  var myLineChart = new Chart (ctxL, {
    type: 'line',
    data: {
      labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      datasets: [{
          label: "Users",
          data: [65, 59, 80, 81, 56, 55, 40, 50, 60, 75, 80, 101],
          backgroundColor: [
            'rgba(105, 0, 132, .2)',
          ],
          borderColor: [
            'rgba(200, 99, 132, .7)',
          ],
          borderWidth: 2
        },
        {
          label: "Orders",
          data: [28, 48, 40, 19, 86, 27, 90, 100, 70, 80, 40, 110],
          backgroundColor: [
            'rgba(0, 137, 132, .2)',
          ],
          borderColor: [
            'rgba(0, 10, 130, .7)',
          ],
          borderWidth: 2
        }
      ]
    },

    options: {
      responsive: true,
    }
  });

});

//collapse sidebar
$(document).ready(function() {
  $('#button-menu').on('click', function(e) {
    e.preventDefault();
    
    $('#sidebar').toggleClass('active');
  });  
});
