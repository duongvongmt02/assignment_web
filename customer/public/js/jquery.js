$(function () {
	$('html,body').scrollspy({target:'body'});
	new WOW().init();
	$('#goTop').click(function(event){
		$('html,body').animate({scrollTop:0});
		return false;
	});
	$('.food').on('mousemove', function(evt){ 
		var windowWidth = $(window).width();
		// console.log("windowWidth: " + windowWidth);
		var cartWidth = $('.iFood').length * 200;
		// console.log("cartWidth:" + cartWidth);
		if(windowWidth < cartWidth){
			// console.log(evt.clientX);
			$('.food').stop(false,true).animate({
				left: - (evt.clientX / windowWidth) * (cartWidth - windowWidth)},"slow");
		}else{
			$('.food').stop(false, true).css({
				left: "0"
			});
		}

	});
	$('.mnImage').click(function(e){
		e.preventDefault();
		var fig = $('.main-img').children('figure');
		var img = $(this).children('img').attr('src');
		fig.children('img').attr('src',img);
		fig.addClass('leftToRight').one('webkitAnimationEnd', function(event){
			$(this).removeClass('leftToRight');
		});
		fig.css({'background':'url('+img+') no-repeat center center /200%'});
		$('.active').removeClass('active');
		$(this).children('img').addClass('active');
	});
	$('.btnSubtract').click(function(e){
		e.preventDefault();
		var q = parseInt($('.btnQuantity').text());
		q>1?q=q-1:q;
		$('.btnQuantity').text(q);
	});
	$('.btnAdd').click(function(e){
		e.preventDefault();
		var q = parseInt($('.btnQuantity').text());
		q<50?q=q+1:q;
		$('.btnQuantity').text(q);
	});
	$('#btnDesc').click(function(e){
		e.preventDefault();
		$('.comments').hide();
		$('.description').show();
	});
	$('#btnComments').click(function(e){
		e.preventDefault();
		$('.comments').show();
		$('.description').hide();
	});
	$('.btnAddToCart').click(function(e){
		e.preventDefault();
		var c = parseInt($('.cartCount').children('span').text());
		c = c+1;
		$('.cartCount').children('span').text(c);
	});
	$('#aClickReview').click(function(event) {
		var vitri = $(this).attr('href');
		var toado = $(vitri).offset().top;
		console.log(toado);
		$('html,body').animate({scrollTop:toado});
		return false;
	});
	$('#btnX').click(function (event) {
		$('.divrating').hide();
		$('.divrating2').hide();
	});
	$('#btnReview').click(function(event){
		$('.divrating').show();
		$('.divrating2').show();
	});
	//multiple item per slide
	$('#recipeCarousel').carousel({
		interval: false
	});
	$('.btn-show-order').click(function(event) {
		$('.idown').toggle("slow");
		$('.iup').toggle("slow");
	});
});
function zoom(e) {
	var zoomer = e.currentTarget;
	var x = (e.offsetX / zoomer.offsetWidth) * 100;
	var y = (e.offsetY / zoomer.offsetHeight) * 100;
	zoomer.style.backgroundPosition = x + "% " + y + "%";
}